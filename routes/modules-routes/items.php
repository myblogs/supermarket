<?php
Route::prefix("items")->name("items.")->group(function(){
    Route::get("{item}/delete","ItemController@delete")
            ->name("delete");
    Route::get("{deletedItem}/restore","ItemController@restore")
            ->name("restore");
    Route::get("search","ItemController@search")
            ->name("search");
    
});
Route::resource("items","ItemController");