<?php

Route::prefix("users")->name("users.")->group(function() {
    Route::get("{user}/delete", "UserController@delete")->name("delete");
    Route::get("{deletedUser}/restore", "UserController@restore")
            ->name("restore");
    Route::post("{userId}/roles", "UserController@setRoles")
            ->name("set_roles");
    Route::get("{user}/roles", "UserController@showRoles")
            ->name("show_roles");
    Route::get("search", "UserController@search")
            ->name("search");

    Route::put("editImage", "UserController@editImage")
            ->name("edit-image");

    Route::get("profile", "UserController@getProfile")
            ->name("profile");

    Route::put("profile", "UserController@updateProfile")
            ->name("profile");
    Route::get("", "UserController@index")->name("index");
});
