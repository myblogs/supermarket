<?php

Route::prefix("warehouses")->name("warehouses.")->group(function() {
    Route::get("{warehouse}/delete", "WarehouseController@delete")
            ->name("delete");
    
    Route::get("{deletedWarehouse}/restore", "WarehouseController@restore")
            ->name("restore");
    
    Route::get("search", "WarehouseController@search")
            ->name("search");
});
Route::resource("warehouses", "WarehouseController");
