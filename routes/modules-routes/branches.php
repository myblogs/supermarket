<?php
Route::prefix("branches")->name("branches.")->group(function(){
    Route::get("{branch}/delete","BranchController@delete")->name("delete");
    Route::get("{deletedBranch}/restore","BranchController@restore")->name("restore");
    Route::get("search","BranchController@search")->name("search");
});

Route::resource('branches',"BranchController");