<?php

Route::prefix("clients")->name("clients.")->group(function() {
    Route::get("{client}/delete", "ClientController@delete")->name("delete");
    Route::get("{deletedClient}/restore", "ClientController@restore")
            ->name("restore");
    Route::get("search", "ClientController@search")->name("search");
});
Route::resource("clients", "ClientController");
