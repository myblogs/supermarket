<?php
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

Route::get('locale/{locale}', function ($locale) {
    Session::put('locale',$locale);
    return redirect()->back();
})->name('locale');

Route::get("logout",function(){
    Auth::logout();
    return redirect()->route("login");
})->name("app-logout");

Auth::routes(['verify'=>true]);

Route::get("home","HomeController@index")->name("name");