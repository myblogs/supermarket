<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Branch;
use App\utils\AppDate;
use Illuminate\Database\Eloquent\SoftDeletes;
class Warehouse extends Model {
    use SoftDeletes;
    protected $fillable = [
        'name_ar',
        "name_en",
        "branch_id"
    ];
    protected $appends=['name'];

    public function branch() {
        return $this->belongsTo(Branch::class);
    }

    public function getCreatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['created_at']);
    }

    public function getUpdatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['updated_at']);
    }

    public function getNameAttribute() {
        if (app()->getLocale() === "ar") {
            return $this->attributes['name_ar'];
        }
        return $this->attributes['name_en'];
    }

}
