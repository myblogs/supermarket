<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\utils\AppDate;
use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends Model {
    use SoftDeletes;
    protected $fillable = [
        "name_ar",
        "name_en",
    ];

    public function getCreatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['created_at']);
    }

    public function getUpdatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['updated_at']);
    }
    

}
