<?php

namespace App\Models\Items;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable=['unit_ar','unit_en','capacity'];
}
