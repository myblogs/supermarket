<?php

namespace App\Models\Items;

use Illuminate\Database\Eloquent\Model;
use App\Models\Items\Unit;
use App\utils\AppDate;
use App\Models\Items\SmallestUnit;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model {

    use SoftDeletes;

    protected $fillable = [
        'name_ar',
        'name_en',
        'minimum_quantity',
        'branch_id',
        'selling_price',
        'smallest_unit_id',
    ];
    protected $appends = [
        'for_sell',
        'name',
        'smallest_unit_name',
        'all_units'
    ];

    //Start relations
    public function units() {
        return $this->belongsToMany(Unit::class);
    }

    public function smallest_unit() {
        return $this->belongsTo(SmallestUnit::class);
    }

    //End relations
    //Start Encapsulation
    public function getForSellAttribute() {
        return $this->attributes['selling_price'] !== null;
    }

    public function getCreatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['created_at']);
    }

    public function getUpdatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['updated_at']);
    }

    public function getNameAttribute() {
        if (app()->getLocale() === "ar") {
            return $this->attributes['name_ar'];
        }
        return $this->attributes['name_en'];
    }

    public function getSmallestUnitNameAttribute() {
        if (app()->getLocale() === "ar") {
            return $this->smallest_unit->smallest_unit_ar;
        }
        return $this->smallest_unit->smallest_unit_en;
    }

    public function getAllUnitsAttribute() {
        $units = [];
        if (app()->getLocale() === "ar") {
            foreach ($this->units as $unit) {
                $units[] = [
                    "name" => $unit->unit_ar,
                    "capacity" => $unit->capacity,
                ];
            }
        } else {
            foreach ($this->units as $unit) {
                $units[] = [
                    "name" => $unit->unit_en,
                    "capacity" => $unit->capacity,
                ];
            }
        }
        return $units;
    }

    //End Encapsulation
}
