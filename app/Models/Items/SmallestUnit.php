<?php

namespace App\Models\Items;
use Illuminate\Database\Eloquent\Model;
class SmallestUnit extends Model{
    protected $fillable=[
        'smallest_unit_ar',
        'smallest_unit_en'
    ];
}
