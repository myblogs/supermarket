<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\utils\AppDate;
use App\User;
class Branch extends Model {

    use SoftDeletes;

    protected $fillable = ["name_ar", "name_en"];
    protected $appends=['name'];
    public function getCreatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['created_at']);
    }

    public function getUpdatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['updated_at']);
    }

    public function getNameAttribute() {
        if (app()->getLocale() === "ar") {
            return $this->attributes['name_ar'];
        }
        return $this->attributes['name_en'];
    }
    public function users(){
        return $this->hasMany(User::class);
    }
}
