<?php

namespace App\Http\Requests\users;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name_ar' => ['required', 'string', 'max:30'],
            'name_en' => ['required', 'string', 'max:30'],
            'phone' => ["required", "numeric", "digits:12"],
            'branch_id' => ['required', "numeric"],
        ];
    }

}
