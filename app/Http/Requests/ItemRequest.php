<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name_ar' => 'required|string|max:30',
            'name_en' => 'required|string|max:30',
            'minimum_quantity'=>'required|integer|min:1',
            'branch_id'=>'required|numeric',
            'selling_price'=>'required_if:for_sell,1|numeric|min:0',
            'smallest_unit_ar'=>'required|string|max:30',
            'smallest_unit_en'=>'required|string|max:30',
            'unit_ar.*'=>'required|string|max:30',
            'unit_en.*'=>'required|string|max:30',
            'capacity.*'=>'required|integer|min:2',
        ];
    }
}
