<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\UserRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\users\ImageRequest;
use App\Repository\BranchRepository;
use App\Http\Requests\users\UserRequest;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller {

    private $userRepository;
    private $branchRepository;

    const PAGE_SIZE = 12;

    function __construct(UserRepository $userRepository, BranchRepository $branchRepositry) {
        $this->userRepository = $userRepository;
        $this->branchRepository = $branchRepositry;
        $this->middleware("auth");
        $this->middleware("verified");
    }

    public function index() {
        $this->authorize("view","users");
        $users = $this->userRepository->getUsers(self::PAGE_SIZE);
        return view("users.managing")->with("users", $users);
    }

    public function setRoles(Request $request, $userId) {
        $this->authorize("update","users");
        $this->userRepository->setRoles($request, $userId);
        return __('site.created_successfully');
    }

    public function showRoles(User $user) {
        $this->authorize("update","users");
        return $user->roles;
    }

    public function delete(User $user) {
        $this->authorize("delete","users");
        $user->delete();
        return __('site.deleted_successfully');
    }

    public function restore($deletedUser) {
        $this->authorize("delete","users");
        $deletedUser->restore();
    }

    public function search(Request $request) {
        $this->authorize("view","users");
        $users = $this->userRepository->search($request, self::PAGE_SIZE);
        return view("users.managing")->with("users", $users);
    }

    public function editImage(ImageRequest $request) {
        $user = Auth::user();
        $this->deleteOldImage($user);
        $this->userRepository->editImage($user, $request);
        return [
            'message' => __('site.updated_successfully'),
            'uploaded_image' => $user->image
        ];
    }

    public function getProfile() {
        $branches = $this->branchRepository->getBranchesWithUsers();
        return view("users.profile")
                        ->with("branches", $branches);
    }

    public function updateProfile(UserRequest $request) {
        $user = $this->userRepository->updateProfile($request);
        return [
            "message" => __('site.updated_successfully'),
            "updatedName" => $user['name']
        ];
    }

    //utils
    private function deleteOldImage($user) {
        $userImagesPath = config('filesystems.userImagesPath');
        $defaultImage = $userImagesPath . "/avatar.png";
        if ($user->image !== $defaultImage) {
            Storage::delete(str_replace($userImagesPath, "", $user->image));
        }
    }

}
