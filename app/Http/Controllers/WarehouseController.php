<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\WarehouseRequest;
use App\Repository\BranchRepository;
use App\Repository\WarehouseRepository;
use App\Models\Warehouse;

class WarehouseController extends Controller {

    private $branchRepository;
    private $warehouseRepository;

    const PAGE_SIZE = 12;

    function __construct(BranchRepository $branchRepository
    , WarehouseRepository $warehouseRepository) {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->branchRepository = $branchRepository;
        $this->warehouseRepository = $warehouseRepository;
    }

    public function index() {
        $this->authorize("view", "warehouses");
        $branches = $this->branchRepository->getAllBranches();
        $warehouses = $this->warehouseRepository
                ->getWarehousesWithBranches(self::PAGE_SIZE);
        return view("warehouses.managing")
                        ->with("branches", $branches)
                        ->with("warehouses", $warehouses);
    }

    public function store(WarehouseRequest $request) {
        $this->authorize("create", "warehouses");
        $warehouse = $this->warehouseRepository->create($request);
        return [
            "message" => __("site.created_successfully"),
            "warehouse" => $warehouse,
            "page_size" => self::PAGE_SIZE
        ];
    }

    public function edit(Warehouse $warehouse) {
        $this->authorize("update", "warehouses");
        return $warehouse;
    }

    public function update(WarehouseRequest $request, $id) {
        $this->authorize("update", "warehouses");
        $warehouse = $this->warehouseRepository->update($request, $id);
        return [
            "warehouse" => $warehouse,
            "message" => __('site.updated_successfully'),
        ];
    }

    public function delete(Warehouse $warehouse) {
        $this->authorize("delete", "warehouses");
        $this->warehouseRepository->delete($warehouse);
        return __("site.deleted_successfully");
    }

    public function restore($deletedWarehouse) {
        $this->authorize("delete", "warehouses");
        $this->warehouseRepository->restore($deletedWarehouse);
    }

    public function search(Request $request) {
        $this->authorize("view", "warehouses");
        $branches = $this->branchRepository->getAllBranches();
        $warehouses = $this->warehouseRepository->search($request
                , self::PAGE_SIZE);
        return view("warehouses.managing")
                        ->with("branches", $branches)
                        ->with("warehouses", $warehouses);
    }

}
