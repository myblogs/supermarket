<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;
use App\Repository\BranchRepository;
use App\Models\Branch;

class BranchController extends Controller {

    private $branchRepository;

    const PAGE_SIZE = 12;

    function __construct(BranchRepository $branchRepository) {
        $this->branchRepository = $branchRepository;
        $this->middleware("auth");
        $this->middleware("verified");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize("view","branches");
        $branches = $this->branchRepository->getBranches(self::PAGE_SIZE);
        return view("branches.managing")
                ->with("branches", $branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request) {
        $this->authorize("create","branches");
        $branch = $this->branchRepository->create($request);
        return [
            "message" => __("site.created_successfully"),
            "branch" => $branch,
            "page_size" => self::PAGE_SIZE
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch) {
        $this->authorize("update","branches");
        return $branch;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $request, $id) {
        $this->authorize("update","branches");
        $branch = $this->branchRepository->update($request, $id);
        return [
            "message" => __('site.updated_successfully'),
            "branch" => $branch
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Branch $branch) {
        $this->authorize("delete","branches");
        $branch->delete();
        return __('site.deleted_successfully');
    }

    public function restore($deletedBranch) {
        $this->authorize("delete","branches");
        $deletedBranch->restore();
    }

    public function search(Request $request) {
        $this->authorize("view","branches");
        $branches = $this->branchRepository->search($request, self::PAGE_SIZE);
        return view("branches.managing")
                        ->with("branches", $branches);
    }

}
