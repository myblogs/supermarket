<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use App\Repository\ClientRepository;
use App\Models\Client;

class ClientController extends Controller {

    private $clientRepository;

    const PAGE_SIZE = 12;

    function __construct(ClientRepository $clientRepository) {
        $this->middleware("auth");
        $this->middleware('verified');
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize("view", "clients");
        $clients = $this->clientRepository->getClients(self::PAGE_SIZE);
        return view("clients.managing")->with("clients", $clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request) {
        $this->authorize("create", "clients");
        $client = $this->clientRepository->create($request);
        return [
            'message' => __("site.created_successfully"),
            'client' => $client,
            "page_size" => self::PAGE_SIZE
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client) {
        $this->authorize("update", "clients");
        return $client;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, $id) {
        $this->authorize("update", "clients");
        $client = $this->clientRepository->update($request, $id);
        return [
            'message' => __("site.updated_successfully"),
            'client' => $client,
        ];
    }

    public function delete(Client $client) {
        $this->authorize("delete", "clients");
        $client->delete();
        return __("site.deleted_successfully");
    }

    public function restore($deletedClient) {
        $this->authorize("delete", "clients");
        $deletedClient->restore();
    }

    public function search(Request $request) {
        $this->authorize("view","clients");
        $clients = $this->clientRepository
                ->search($request, self::PAGE_SIZE);
        return view("clients.managing")
                ->with("clients",$clients);
    }

}
