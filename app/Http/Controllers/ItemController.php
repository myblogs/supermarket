<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use App\Repository\ItemRepository;
use App\Repository\BranchRepository;
use App\Models\Items\Item;
use Illuminate\Http\Request;

class ItemController extends Controller {

    private $itemRepository;
    private $branchRepository;

    const PAGE_SIZE = 12;

    function __construct(ItemRepository $itemRepository
    , BranchRepository $branchRepository) {
        $this->middleware("auth");
        $this->middleware('verified');
        $this->itemRepository = $itemRepository;
        $this->branchRepository = $branchRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize("view", "items");
        $branches = $this->branchRepository->getAllBranches();
        $items = $this->itemRepository->getItemsWithUnits(self::PAGE_SIZE);
        return view("items.managing")
                        ->with("branches", $branches)
                        ->with("items", $items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request) {
        $this->authorize("create", "items");
        $item = $this->itemRepository->create($request);
        return [
            'message' => __('site.created_successfully'),
            "item" => $item,
            "page_size" => self::PAGE_SIZE
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item) {
        $this->authorize("update", "items");
        return [
            "item" => $item,
            "smallest_unit" => $item->smallest_unit,
            "units" => $item->units
        ];
    }

    /*     * S
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(ItemRequest $request, $id) {
        $this->authorize("update", "items");
        $item = $this->itemRepository->update($request, $id);
        return [
            'message' => __('site.updated_successfully'),
            'item' => $item,
        ];
    }

    public function delete(Item $item) {
        $this->authorize("delete", "items");
        $item->delete();
        return __('site.deleted_successfully');
    }

    public function restore($deletedItem) {
        $this->authorize("delete", "items");
        $deletedItem->restore();
    }

    public function search(Request $request) {
        $this->authorize("view", "items");
        $branches = $this->branchRepository->getAllBranches();
        $items = $this->itemRepository->search($request, self::PAGE_SIZE);
        return view("items.managing")
                        ->with("branches", $branches)
                        ->with("items", $items);
    }

}
