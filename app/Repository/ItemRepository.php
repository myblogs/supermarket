<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use App\Models\Items\Item;
use App\Models\Items\SmallestUnit;
use App\Models\Items\Unit;

/**
 * Description of UserRepository
 *
 * @author Ahmed
 */
class ItemRepository {

    public function create($request) {
        $smallestUnitId = $this->createSmallestUnit($request);
        $unitIds = $this->createUnits($request);
        $item = $this->createItem($request, $smallestUnitId, $unitIds);
        return $item;
    }

    public function update($request, $id) {
        $smallestUnitId = $this->createSmallestUnit($request);
        $unitIds = $this->createUnits($request);
        $item = $this->updateItem($request, $id, $smallestUnitId, $unitIds);
        return $item;
    }

    public function getItemsWithUnits($pageSize) {
        return Item::withTrashed()
                        ->with("units")
                        ->with("smallest_unit")
                        ->simplePaginate($pageSize);
    }

    public function getDeletedItem($id) {
        return Item::onlyTrashed()->find($id) ?? abort(404);
    }

    public function search($request, $pageSize) {
        $item = Item::withTrashed()->where('id', $request->search)
                ->orWhere("name_ar", "like", "%$request->search%")
                ->orWhere("name_en", "like", "%$request->search%")
                ->simplePaginate($pageSize);
        $item->appends(['search' => $request->search]);
        return $item;
    }

    //utils
    private function createSmallestUnit($request) {
        $smallestUnitInputs = [];
        $smallestUnitInputs['smallest_unit_ar'] = $request
                ->input('smallest_unit_ar');
        $smallestUnitInputs['smallest_unit_en'] = $request
                ->input('smallest_unit_en');
        $smallestUnit = SmallestUnit::updateOrCreate($smallestUnitInputs);
        return $smallestUnit['id'];
    }

    private function createUnits($request) {
        $inputs = [];
        $ids = [];
        $length = $request->capacity ? count($request->capacity) : 0;
        for ($i = 0; $i < $length; $i++) {
            $inputs['unit_ar'] = $request->unit_ar[$i];
            $inputs['unit_en'] = $request->unit_en[$i];
            $inputs['capacity'] = $request->capacity[$i];
            $unit = Unit::updateOrCreate($inputs);
            $ids[] = $unit->id;
        }
        return $ids;
    }

    private function createItem($request, $smallestUnitId, $unitIds) {
        $itemInputs = $request->input();
        $itemInputs['smallest_unit_id'] = $smallestUnitId;
        if (!$request->selling_price) {
            $itemInputs['selling_price'] = null;
        }
        $item = Item::create($itemInputs);
        $item->units()->sync($unitIds);
        return $item;
    }

    private function updateItem($request, $id, $smallestUnitId, $unitIds) {
        $item = Item::find($id);
        $item['name_ar'] = $request->name_ar;
        $item['name_en'] = $request->name_en;
        $item['minimum_quantity'] = $request->minimum_quantity;
        $item['branch_id'] = $request->branch_id;
        $item['selling_price'] = $request->selling_price;
        $item['smallest_unit_id'] = $smallestUnitId;
        $item->units()->sync($unitIds);
        $item->save();
        return $item;
    }

}
