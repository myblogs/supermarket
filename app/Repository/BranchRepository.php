<?php

namespace App\Repository;

use App\Models\Branch;

class BranchRepository {

    public function create($request) {
        return Branch::create($request->input());
    }

    public function getBranches($pageSize) {
        return Branch::withTrashed()->simplePaginate($pageSize);
    }

    public function update($request, $id) {
        $branch = Branch::find($id);
        $inputs = $request->input();
        $branch->name_ar = $inputs['name_ar'];
        $branch->name_en = $inputs['name_en'];
        $branch->save();
        return $branch;
    }

    public function search($request, $pageSize) {
        $branches=Branch::withTrashed()->where('id',$request->search)
                ->orWhere("name_ar","like","%$request->search%")
                ->orWhere("name_en","like","%$request->search%")
                ->simplePaginate($pageSize);
        $branches->appends(['search'=>$request->search]);
        return $branches;
    }
    
    public function getAllBranches(){
        return Branch::get();
    }
    
    public function getBranchesWithUsers(){
        return Branch::with("users")->get();
    }
    
    public function getDeletedBranch($id){
         return Branch::onlyTrashed()->find($id)?? abort(404);
    }
    public function isIn($userId,$branch){
        return $branch
                ->users()
                ->where('id',$userId)
                ->count()>0?true:false;
    }
}
