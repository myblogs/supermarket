<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Builder;

/**
 * Description of UserRepository
 *
 * @author Ahmed
 */
class WarehouseRepository {

    public function create($request) {
        $warehouse = Warehouse::create($request->input());
        $warehouse->branch;
        return $warehouse;
    }

    public function getWarehousesWithBranches($pageSize) {
        return Warehouse::withTrashed()->with("branch")->simplePaginate($pageSize);
    }

    public function update($request, $id) {
        $warehouse = Warehouse::find($id);
        $warehouse['name_ar'] = $request->name_ar;
        $warehouse['name_en'] = $request->name_en;
        $warehouse['branch_id'] = $request->branch_id;
        $warehouse->save();
        $warehouse->branch;
        return $warehouse;
    }

    public function delete($warehouse) {
        $warehouse->delete();
    }

    public function getDeletedWarehouse($id) {
        return Warehouse::onlyTrashed()->find($id) ?? abort(404);
    }

    public function restore($deletedWarehouse) {
        $deletedWarehouse->restore();
    }

    public function search($request, $pageSize) {
        $warehouses = Warehouse::withTrashed()
                ->where('id', $request->search)
                ->orWhere("name_ar", "like", "%$request->search%")
                ->orWhere("name_en", "like", "%$request->search%")
                ->orWhereHas("branch", function(Builder $query)use ($request) {
                    $query->where("name_ar", "like", "%$request->search%")
                    ->orWhere("name_en", "like", "%$request->search%");
                })
                ->simplePaginate($pageSize);
        $warehouses->appends(['search' => $request->search]);
        return $warehouses;
    }

}
