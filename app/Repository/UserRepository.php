<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

/**
 * Description of UserRepository
 *
 * @author Ahmed
 */
class UserRepository {

    public function getUsers($pageSize) {
        return User::withTrashed()
                        ->where("email", "<>", "superadmin@supermarket.com")
                        ->where("id", "<>", Auth::id())
                        ->simplePaginate($pageSize);
    }

    public function getDeletedUser($id) {
        return User::onlyTrashed()->find($id) ?? abort(404);
    }

    public function createSuperAdminUser() {
        User::create([
            'name_ar' => "سوبر ادمن",
            'name_en' => "Super admin",
            'email' => "superadmin@supermarket.com",
            'password' => Hash::make("12345678"),
            "email_verified_at" => date("Y-m-d H:i:s", strtotime('now')),
        ]);
    }

    public function setRoles($request, $userId) {
        User::find($userId)
                ->roles()
                ->sync($request->roles);
    }

    public function search($request, $pageSize) {
        $users = User::where("id", $request->search)
                ->orWhere('name_ar', "like", "%$request->search%")
                ->orWhere('name_en', "like", "%$request->search%")
                ->orWhere('email', $request->search)
                ->orWhere('phone', "like", "%$request->search%")
                ->simplePaginate($pageSize);
        $users->appends(['search' => $request->search]);
        return $users;
    }

    public function editImage($user, $request) {
        $user->image = $request->image->store('');
        $user->save();
    }

    public function updateProfile($request) {
        $user = Auth::user();
        return User::updateOrCreate(["id" => $user['id']], $request->input());
    }

    public function hasPermission($user,$roleName) {
        return $user->roles()
                        ->where("name",$roleName)
                        ->count() > 0 ? true : false;
    }

}
