<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use App\Models\Client;

/**
 * Description of UserRepository
 *
 * @author Ahmed
 */
class ClientRepository {

    public function create($request) {
        return Client::create($request->input());
    }

    public function getClients($pageSize) {
        return Client::withTrashed()
                        ->simplePaginate($pageSize);
    }

    public function update($request, $id) {
        $client = Client::find($id);
        $client->name_ar = $request->name_ar;
        $client->name_en = $request->name_en;
        $client->save();
        return $client;
    }

    public function getDeletedClient($id) {
        return Client::onlyTrashed()->find($id) ?? abort(404);
    }

    public function search($request,$pageSize) {
        $client = Client::withTrashed()->where('id', $request->search)
                ->orWhere("name_ar", "like", "%$request->search%")
                ->orWhere("name_en", "like", "%$request->search%")
                ->simplePaginate($pageSize);
        $client->appends(['search' => $request->search]);
        return $client;
    }

}
