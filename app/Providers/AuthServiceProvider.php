<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Repository\UserRepository;

class AuthServiceProvider extends ServiceProvider {

    private $userRepository;

    function __construct() {
        $this->userRepository = new UserRepository();
    }

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
            // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() {
        $this->registerPolicies();
        $this->createGates();
    }

    //Utils
    private function createGates() {
        $methods = ["create", "update", "delete", "view"];
        foreach ($methods as $method) {
            Gate::define($method, function($user, $model) use($method) {
                return $user->email==="superadmin@supermarket.com"
                        ||$this->userRepository
                                ->hasPermission($user, $method . "_" . $model);
            });
        }
    }

}
