<?php

namespace App\Providers\users;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\Repository\BranchRepository;
class AppServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        $this->isIn();
    }

    //Utils
    private function isIn() {
        Blade::if('isIn', function ($userId, $branch) {
        return (new BranchRepository())->isIn($userId, $branch);
        });
    }

}
