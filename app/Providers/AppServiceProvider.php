<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        View::composer('*', function ($view) {
            $view->with("currentUser", Auth::user());
        });
        DB::listen(function ($query) {
//            echo "query : $query->sql<br>";
//            echo "time : $query->time<br>";
        });
        
    }

}
