<?php

namespace App\utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Encapsulation
 *
 * @author Ahmed
 */
class AppDate {

    public static function getDiff($date1, $date2) {
        $diff = abs(strtotime($date1) - strtotime($date2));
        $years = (int) floor($diff / (365 * 60 * 60 * 24));
        $months = (int) floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = (int) floor(($diff - $years * 365 * 60 * 60 * 24 -
                        $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = (int) floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = (int) floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = (int) floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
        return compact("years", "months", "months", "days", "hours", "minutes", "seconds");
    }

    public static function getDiffWithCurrentDate($date) {
        $diff = self::getDiff(date("Y-m-d H:i:s"), $date);
        if ($diff['years'] !== 0) {
            return trans_choice('site.year', $diff['years']);
        }
        if ($diff['months'] !== 0) {
            return trans_choice('site.month', $diff['months']);
        }
        if ($diff['days'] !== 0) {
            return trans_choice('site.day', $diff['days']);
        }

        if ($diff['hours'] !== 0) {
            return trans_choice('site.hour', $diff['hours']);
        }

        if ($diff['minutes'] !== 0) {
            return trans_choice('site.minute', $diff['minutes']);
        }
        
        return trans_choice('site.second', $diff['seconds']);
    }

}
