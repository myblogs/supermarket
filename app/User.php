<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MailResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\SendVerifyEmailNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\utils\AppDate;
use App\Models\Role;
use App\Models\Branch;
class User extends Authenticatable implements MustVerifyEmail {

    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_ar', "name_en", 'email', 'password', "phone", "branch_id"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute() {
        if (app()->getLocale() === "ar") {
            return $this->attributes['name_ar'];
        }
        return $this->attributes['name_en'];
    }

    public function getCreatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['created_at']);
    }
    public function getUpdatedAtAttribute() {
        return AppDate::getDiffWithCurrentDate($this->attributes['updated_at']);
    }

    public function sendPasswordResetNotification($token) {
        $this->notify(new MailResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification() {
        $this->notify(new SendVerifyEmailNotification);
    }

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function getImageAttribute() {
        $userImages = "images/users";
        $image = $this->attributes['image'];
        return $image ? "$userImages/$image" : "$userImages/avatar.png";
    }
    public function branch(){
        return $this->belongsTo(Branch::class);
    }
}
