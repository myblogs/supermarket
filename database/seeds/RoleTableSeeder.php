<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $roles = ["create", "update", "delete", "view"];
        $models = ["users", "branches","clients","items","warehouses"];
        foreach ($models as $model) {
            foreach ($roles as $role) {
                Role::create([
                    'name' => $role . "_" . $model
                ]);
            }
        }
    }

}
