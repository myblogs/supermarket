<?php

use Illuminate\Database\Seeder;
use App\Repository\UserRepository;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new UserRepository())->createSuperAdminUser();
    }
}
