<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->softDeletes();
            $table->string("name_ar")->nullable();
            $table->string("name_en")->nullable();
            $table->integer("minimum_quantity")->nullable();
            $table->decimal("selling_price")->nullable();
            $table->unsignedBigInteger('branch_id');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->unsignedBigInteger('smallest_unit_id');
            $table->foreign('smallest_unit_id')->references('id')->on('smallest_units');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('items');
    }

}
