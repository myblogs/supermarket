<div id="{{$id}}" class="modal fade" role="dialog">
    <form id="form">
        @csrf
        {!! $putInput !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{$action}}</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>@lang('site.name-ar')</label>
                        <input class="form-control" name="name_ar" type="text"/>
                        <div id="name-ar-error" class="text-danger">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>@lang('site.name-en')</label>
                        <input class="form-control" name="name_en" type="text"/>
                        <div id="name-en-error" class="text-danger">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>@lang('site.minimum-quantity')</label>
                        <input name="minimum_quantity" class="form-control" type="number"/>
                        <div id="minimum-quantity-error" class="text-danger">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>@lang('site.branches')</label>
                        <select class="form-control" name='branch_id'>
                            <option value="">@lang('site.branches')</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch['id']}}">
                                {{$branch['name']}}
                            </option>
                            @endforeach
                        </select>
                        <div id="branch-error" class="text-danger">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>@lang('site.selling-price')</label>
                        <input name="selling_price" disabled name="selling_price" class="form-control" type="text"/>
                        <div id="selling-price-error" class="text-danger">
                        </div>
                    </div>
                    <div class="form-group">
                        <label id='checkbox'>
                            <input name="for_sell" 
                                   type="checkbox"
                                   onchange="utils.togglePriceInputEnabling($(this),'#{{$id}}')"
                                   >
                            @lang('site.for-sell') 
                        </label>
                    </div>
                    <div class='row'>
                        <div class='col-md-4'>
                            <div class="form-group">
                                <label>@lang('site.smallest-unit-ar')</label>
                                <input name="smallest_unit_ar" class="form-control" type="text" >
                                <div id="smallest-unit-ar-error" class="text-danger">
                                </div>
                            </div>

                        </div>
                        <div class='col-md-4'>
                            <div class="form-group">
                                <label>@lang('site.smallest-unit-en')</label>
                                <input name="smallest_unit_en" class="form-control" type="text">
                                <div id="smallest-unit-en-error" class="text-danger">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id='units'>

                    </div>
                    <button  id="add" type="button" class="btn btn-primary btn-sm">
                        <i class='fa fa-plus'></i>
                    </button>

                    <button id="remove" type="button" class="btn btn-primary btn-sm">
                        <i class='fa fa-minus'></i>
                    </button>

                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="{{$action}}">
                    <a class="btn btn-warning" data-dismiss="modal">@lang('site.close')</a>
                </div>
            </div>
        </div>
    </form>
</div>