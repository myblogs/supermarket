@extends('layouts.app')
@section("title",__('site.items'))
@section('content')

@push('items-script')
@include('js.items.boundary')
@endpush

@include('items.modal',[
'id'=>'create-item',
'action'=>__('site.create'),
'putInput'=>'',
])

@include('items.modal',[
"id"=>'update-item',
"action"=>__('site.update'),
"putInput"=>"<input type='hidden' name='_method' value='put'>",
])

@include('includes.delete')
<section class="content-header">
    <h1>
        @lang('site.items')
    </h1>
    <div class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @lang('site.managing') @lang('site.items')
                </h3>
                <div class="box-tools">
                    <form method="get" action='{{route('items.search')}}'>
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <input type="text" name="search" class="form-control pull-right" placeholder="@lang('site.search')">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="myTable" class="table no-margin">
                        <thead>
                            <tr>
                                <th>@lang('site.id')</th>
                                <th>@lang('site.name')</th>
                                <th>@lang('site.existence')</th>
                                <th>@lang('site.minimum-quantity')</th>
                                <th>@lang('site.selling-price')</th>
                                <th>@lang('site.smallest-unit')</th>
                                <th>@lang('site.units')</th>
                                <th>@lang('site.creation-date')</th>
                                <th>@lang('site.last-update')</th>
                                <th>@lang('site.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                            <tr id="{{$item['id']}}">
                                <td>{{$item['id']}}</td>
                                <td>{{$item['name']}}</td>
                                <td>
                                    @if($item->trashed())
                                    <span class='label label-danger'>
                                        @lang('site.deleted')
                                    </span>
                                    @else
                                    <span class='label label-success'>
                                        @lang('site.exist')
                                    </span>
                                    @endif
                                </td>
                                <td>{{$item['minimum_quantity']}}</td>
                                <td>
                                    @if($item['for_sell'])
                                    {{$item['selling_price']}}
                                    @else
                                    <span class='label label-warning'>
                                        @lang('site.not-for-sell')
                                    </span>
                                    @endif
                                </td>
                                <td>{{$item['smallest_unit_name ']}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                            @lang('site.units')
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            @foreach($item->all_units as $unit)
                                            <li>
                                                <a>
                                                    {{
                                                    $unit['name']." = "
                                                    .$unit['capacity']." "
                                                    .$item['smallest_unit_name']
                                                    }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </td>
                                <td>{{$item['created_at']}}</td>
                                <td>
                                    {{$item['updated_at']}}
                                </td>
                                <td>
                                    @if($item->trashed())
                                    <button
                                        class="btn btn-default btn-sm"
                                        onclick="requests.restore({{$item['id']}})"
                                        @cannot('delete','items') disabled @endcannot
                                        >
                                        <i class="fa fa-window-restore"></i>
                                    </button>

                                    @else
                                    <button
                                        onclick='requests.getItem({{$item['id']}})'
                                        data-toggle="modal"
                                        data-target="#update-item"
                                        class="btn btn-default btn-sm"
                                        @cannot('update','items') disabled @endcannot
                                        >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    
                                    <button
                                        onclick="utils.confirmDelete({{$item['id']}})" 
                                        data-target="#confirm-delete" 
                                        data-toggle="modal" 
                                        class="btn btn-default btn-sm"
                                        @cannot('delete','items') disabled @endcannot
                                        >
                                        <i class='fa fa-trash'></i>
                                    </button>
                                    
                                    @endif
                                </td>
                            </tr>
                            
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <button 
                    data-target='#create-item' 
                    data-toggle='modal'
                    class='btn btn-primary'
                    @cannot('create','items') disabled @endcannot
                    >
                    @lang('site.create')
            </button>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
    {{$items->links()}}
</div>
</section>
@endsection