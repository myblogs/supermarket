<div id="authorizations" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('site.authorizations')</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button id="button" type="button" class="btn btn-primary">@lang('site.create')</button>
                    <a class="btn btn-warning" data-dismiss="modal">@lang('site.close')</a>
                </div>
            </div>
        </form>

    </div>
</div>
