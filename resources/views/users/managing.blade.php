@extends('layouts.app')
@section('title',__('site.users'))
@section('content')
@push('user-scripts')
@include('js.users.boundary')
@endpush

@push('users-styles')
<link rel="stylesheet" href="{{asset('css/users/users.css')}}"/>
@endpush

@include('includes.delete')
@include('users.authorization')
<section class="content-header">
    <h1>
        @lang('site.users')
    </h1>
    <div class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @lang('site.managing') @lang('site.users')
                </h3>
                <div class="box-tools">
                    <form method="get" action="{{route('users.search')}}">
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <input type="text" name="search" class="form-control pull-right" placeholder="@lang('site.search')">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="myTable" class="table no-margin">
                        <thead>
                            <tr>
                                <th>@lang('site.id')</th>
                                <th>@lang('site.name')</th>
                                <th>@lang('site.existence')</th>
                                <th>@lang('site.activation')</th>
                                <th>@lang('site.creation-date')</th>
                                <th>@lang('site.phone')</th>
                                <th>@lang('site.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user['id']}}</td>
                                <td>{{$user['name']}}</td>
                                <td id='existence-{{$user['id']}}'>
                                    @if($user->trashed())
                                    <span class='label label-danger'>
                                        @lang('site.deleted')
                                    </span>
                                    @else
                                    <span class='label label-success'>
                                        @lang('site.exist')
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    @if($user->hasVerifiedEmail())
                                    <span class='label label-success'>
                                        @lang('site.activated')
                                    </span>
                                    @else
                                    <span class='label label-danger'>
                                        @lang('site.non-activated')
                                    </span>
                                    @endif
                                </td>
                                <td>{{$user['created_at']}}</td>
                                <td>{{$user['phone']}}</td>
                                <td id='actions-{{$user['id']}}'>
                                    @if($user->trashed())
                                    <button onclick="requests.restore({{$user['id']}})" 
                                            class="btn btn-default btn-sm"
                                            @cannot('delete','users') disabled @endcannot
                                            >
                                        <i class="fa fa-window-restore"></i>
                                    </button>
                                    @else
                                    <button
                                        onclick="requests.showRoles({{$user['id']}})" 
                                        data-target="#authorizations" 
                                        data-toggle="modal" 
                                        class="btn btn-default btn-sm" 
                                        @cannot('update','users') disabled @endcannot
                                        >
                                        <i class='fa fa-lock'></i>
                                    </button>
                                    <button
                                        onclick="utils.confirmDelete({{$user['id']}})" 
                                        data-target="#confirm-delete" 
                                        data-toggle="modal" 
                                        class="btn btn-default btn-sm"
                                        @cannot('delete','users') disabled @endcannot
                                        >
                                        <i class='fa fa-trash'></i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        {{$users->links()}}
    </div>
</section>
@endsection('content')
