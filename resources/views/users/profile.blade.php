@extends('layouts.app')
@section('title',__('site.profile'))
@section('content')
@push('user-scripts')
@include('js.users.boundary')
@endpush
<section class="content-header">
    <h1>@lang('site.profile')</h1>
</section>
<div class="content">
    <div class="box box-default">
        <div class="box-body">
            <form id='profile-update-form'>
                @csrf
                @method('put')
                <div class='form-group'>
                    <label>@lang('site.name-ar')</label>
                    <input class='form-control' type="text"
                           value="{{$currentUser['name_ar']}}" name="name_ar">
                    <div class="text-danger" id="name-ar-error"></div>
                </div>

                <div class='form-group'>
                    <label>@lang('site.name-en')</label>
                    <input class='form-control' type="text" 
                           value="{{$currentUser['name_en']}}" name="name_en">
                    <div class="text-danger" id="name-en-error"></div>
                </div>

                <div class='form-group'>
                    <label>@lang('site.phone')</label>
                    <input class='form-control' type="text" 
                           value="{{$currentUser['phone']}}" name="phone">
                    <div class="text-danger" id="phone-error"></div>
                </div>

                <div class='form-group'>
                    <label>@lang('site.branches')</label>
                    <select class="form-control" name="branch_id">
                        <option value="">@lang('site.branches')</option>
                        @foreach($branches as $branch)
                        <option
                            @isIn($currentUser['id'],$branch)
                            selected
                            @endisIn 
                            value="{{$branch['id']}}">
                            {{$branch['name']}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger" id="branch-error"></div>
            </div>
            <input type="submit" class="btn btn-primary" value="@lang('site.update')">
            <a href="{{URL::previous()}}" class="btn btn-warning">
                @lang('site.back') 
            </a>
        </form>
    </div>
    <!-- /.box-body -->
</div>
</div>

</div>
<!-- /.box -->
</div>
@endsection