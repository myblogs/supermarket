@extends('layouts.app')
@section('content')

@include('warehouses.modal',[
'id'=>'create-warehouse',
'action'=>__('site.create'),
'putInput'=>'',
])

@push('warehouse-script')
@include('js.warehouses.boundary')
@endpush

@include('warehouses.modal',[
"id"=>'update-warehouse',
"action"=>__('site.update'),
"putInput"=>"<input type='hidden' name='_method' value='put'>",
])

@include('includes.delete')

<section class="content-header">
    <h1>
        @lang('site.warehouses')
    </h1>
    <div class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @lang('site.managing') @lang('site.warehouses')
                </h3>
                <div class="box-tools">
                    <form method="get" action='{{route("warehouses.search")}}'>
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <input type="text" name="search" class="form-control pull-right" placeholder="@lang('site.search')">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="myTable" class="table no-margin">
                        <thead>
                            <tr>
                                <th>@lang('site.id')</th>
                                <th>@lang('site.name')</th>
                                <th>@lang('site.existence')</th>
                                <th>@lang('site.branches')</th>
                                <th>@lang('site.creation-date')</th>
                                <th>@lang('site.last-update')</th>
                                <th>@lang('site.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($warehouses as $warehouse)
                            <tr id="{{$warehouse['id']}}">
                                <td>{{$warehouse['id']}}</td>
                                <td>{{$warehouse['name']}}</td>
                                <td>
                                    @if($warehouse->trashed())
                                    <span class='label label-danger'>
                                        @lang('site.deleted')
                                    </span>
                                    @else
                                    <span class='label label-success'>
                                        @lang('site.exist')
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{$warehouse["branch"]["name"]}}
                                </td>
                                <td>
                                    {{$warehouse['created_at']}}
                                </td>
                                <td>
                                    {{$warehouse['updated_at']}}
                                </td>
                                <td>
                                    @if($warehouse->trashed())
                                    <button
                                        data-toggle="modal"
                                        class="btn btn-default btn-sm"
                                        onclick="requests.restore({{$warehouse['id']}})"
                                        @cannot('delete','warehouses') disabled @endcannot
                                        >
                                        <i class="fa fa-window-restore"></i>
                                    </button>
                                    @else
                                    <button
                                        onclick='requests.getWarehouse({{$warehouse['id']}})'
                                        data-toggle="modal"
                                        data-target="#update-warehouse"
                                        class="btn btn-default btn-sm"
                                        @cannot('update','warehouses') disabled @endcannot
                                        >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <button
                                        onclick="utils.confirmDelete({{$warehouse['id']}})" 
                                        data-target="#confirm-delete" 
                                        data-toggle="modal" 
                                        class="btn btn-default btn-sm"
                                        @cannot('delete','warehouses') disabled @endcannot
                                        >
                                        <i class='fa fa-trash'></i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <button data-target="#create-warehouse" 
                        class="btn btn-primary"
                        data-toggle="modal"
                        @cannot('create','warehouses') disabled @endcannot
                        >
                        @lang("site.create")
            </button>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
    {{$warehouses->links()}}
</div>
</section>

@endsection