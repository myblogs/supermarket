<!DOCTYPE html>
<html dir="@lang('site.dir')">
    <head>
        <title>@lang('site.change-password')</title>
        @include('includes.app.styles')
        <link rel="stylesheet" href="{{asset('css/auth/auth.css')}}" />
    </head>
    <body>
        <div class="register-box">
            <div class="register-box-body">
                <p class="login-box-msg">@lang('site.change-password')</p>
                <form method="post" action="{{ route('password.update')}}">
                    <input type="hidden" name="token" value="{{ $token }}"/>
                    @csrf
                    <div class="form-group has-feedback">
                        <input name="email" type="email" class="form-control" value="{{$email}}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @error('email')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group has-feedback">
                        <input name="password" type="password" class="form-control" placeholder="@lang('site.password')">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @error('password')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group has-feedback">
                        <input name="password_confirmation" type="password" class="form-control" placeholder="@lang('site.confirm-password')">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <button id="register-button" type="submit" class="btn btn-primary btn-flat">
                        @lang('site.change-password')
                    </button>
                    @include('includes.languages')
                </form>
                <a href="{{route('login')}}">@lang('site.login-redirect')</a>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->
    </body>
</html>
