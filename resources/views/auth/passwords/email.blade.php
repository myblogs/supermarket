<!DOCTYPE html>
<html dir="@lang('site.dir')">
    <head>
        <title>@lang('site.reset-password')</title>
        @include('includes.app.styles')
        <link rel="stylesheet" href="{{asset('css/auth/auth.css')}}" />
    </head>
    <body class>
        <div class="register-box">
            <div class="register-box-body">
                <p class="login-box-msg">@lang('site.reset-password')</p>
                <form method="post" action='{{route('password.email')}}'>
                    @csrf
                    <div class="form-group has-feedback">
                        <input name="email" type="email" class="form-control" placeholder="@lang('site.email')">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @error('email')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                        @if (session('status'))
                        <div class="text-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                    </div>
                    <button id="register-button" type="submit" class="btn btn-primary btn-flat">
                        @lang('site.reset-password')
                    </button>
                    @include('includes.languages')
                </form>
                <a href="{{route('login')}}">@lang('site.login-redirect')</a>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->
    </body>
</html>
