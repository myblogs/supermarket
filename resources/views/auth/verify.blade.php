<!DOCTYPE html>
<html dir="@lang('site.dir')">
    <head>
        <title>@lang('site.verify-header')</title>
        @include('includes.app.styles')
        <link rel="stylesheet" href="{{asset('css/auth/auth.css')}}" />
    </head>
    <body>
        <div class="register-box">
            <div class="register-box-body">
                <p class="login-box-msg">@lang("site.verify-header")</p>
                <form method="post" action="{{ route('verification.resend') }}">
                    @csrf
                    <p class="text-danger">
                        @lang('site.verify-message')
                    </p>
                    @if (session('resent'))
                    <p class="text-success" >
                        @lang('site.resent-success')
                    </p>
                    @endif
                    @include('includes.languages')
                    <button type="submit" class="btn btn-link">
                        @lang('site.verify')
                    </button>
                </form>
                <div>
                    <a href="{{route('app-logout')}}" class="btn btn-link">@lang('site.logout')</a>
                </div>
            </div>
            <!-- /.form-box -->
        </div>
    </body>
</html>
