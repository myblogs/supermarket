<!DOCTYPE html>
<html dir="@lang('site.dir')">
    <head>
        <title>@lang('site.register')</title>
        @include('includes.app.styles')
        <link rel="stylesheet" href="{{asset('css/auth/auth.css')}}"/>
    </head>
    <body class>
        <div class="register-box">
            <div class="register-box-body">
                <p class="login-box-msg">@lang('site.register-header')</p>
                <form>
                    <div class="form-group has-feedback">
                        <input id="name_ar" type="text" class="form-control" placeholder="@lang('site.name-ar')">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <div id="name-ar-error" class="text-danger"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <input id="name_en" type="text" class="form-control" placeholder="@lang('site.name-en')">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <div id="name-en-error" class="text-danger"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <input id="email" type="email" class="form-control" placeholder="@lang('site.email')">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <div id="email-error" class="text-danger"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="password" type="password" class="form-control" placeholder="@lang('site.password')">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <div id="password-error" class="text-danger"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="password_confirmation" type="password" class="form-control" placeholder="@lang('site.confirm-password')">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input id="phone" type="text" class="form-control" placeholder="@lang('site.phone')">
                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                        <div id="phone-error" class="text-danger"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <select id="branch_id" class="form-control">
                            <option value="">@lang('site.branches')</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch['id']}}">
                                {{$branch['name']}}
                            </option>
                            @endforeach
                        </select>
                        <div id="branch-error" class="text-danger"></div>
                    </div>
                    <button id="register-button" type="button" class="btn btn-primary btn-flat">@lang('site.register')</button>
                    @include('includes.languages')
                </form>
                <a href="{{route('login')}}" class="text-center">@lang('site.login-redirect')</a>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->
        @include('includes.app.scripts')
        @include('js.auth.boundary')
    </body>
</html>
