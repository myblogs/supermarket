<!DOCTYPE html>
<html dir="@lang('site.dir')">
    <head>
        <title>@lang('site.login')</title>
        @include('includes.app.styles')
        <link rel="stylesheet" href="{{asset('css/auth/auth.css')}}" />
    </head>
    <body>
        <div class="register-box">
            <div class="register-box-body">
                <p class="login-box-msg">@lang('site.login')</p>
                <form method="post" action='{{route('login')}}'>
                    @csrf
                    <div class="form-group has-feedback">
                        <input name="email" type="email" class="form-control" placeholder="@lang('site.email')">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @error('email')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input name="password" type="password" class="form-control" placeholder="@lang('site.password')">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @error('password')
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember">
                            @lang('site.remember-me')
                        </label>
                    </div>
                    <button id="register-button" type="submit" class="btn btn-primary btn-flat">@lang('site.login')</button>

                    @include('includes.languages')
                </form>
                <div>
                    <a href="{{route('register')}}" class="text-center">@lang('site.register-redirect')</a>
                </div>
                <div>
                    <a href="{{route('password.request')}}" class="text-center">@lang('site.reset-password')</a>
                </div>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->
    </body>
</html>
