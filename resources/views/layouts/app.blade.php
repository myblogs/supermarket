<!DOCTYPE html>
<html dir="@lang('site.dir')">
    <head>
        <title>@yield('title')</title>
        @include('includes.app.styles')
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            @include('includes.app.user-image')
            @include('includes.app.header')
            @include('includes.app.sidebar')
            <div class="content-wrapper">
                @yield('content')
            </div>
            @include('includes.app.footer')
        </div>
        @include('includes.app.scripts')
        @include('js.app.boundary')
    </body>
</html>
