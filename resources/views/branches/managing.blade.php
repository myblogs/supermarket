@extends('layouts.app')
@section('content')

@push('branch-scripts')
@include('js.branches.boundary')
@endpush

@include('branches.modal',[
"id"=>'create-branch',
"method"=>"post",
'submitButton'=>__('site.create')
])

@include('branches.modal',[
"id"=>'update-branch',
"method"=>'put',
"submitButton"=>__('site.update'),
])

@include('includes.delete')
<section class="content-header">
    <h1>
        @lang('site.branches')
    </h1>
    <div class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @lang('site.managing') @lang('site.branches')
                </h3>
                <div class="box-tools">
                    <form method="get" action='{{route('branches.search')}}'>
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <input type="text" name="search" class="form-control pull-right" placeholder="@lang('site.search')">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="myTable" class="table no-margin">
                        <thead>
                            <tr>
                                <th>@lang('site.id')</th>
                                <th>@lang('site.name')</th>
                                <th>@lang('site.existence')</th>
                                <th>@lang('site.creation-date')</th>
                                <th>@lang('site.last-update')</th>
                                <th>@lang('site.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($branches as $branch)
                            <tr>
                                <td>{{$branch['id']}}</td>
                                @if(app()->getLocale()==="ar")
                                <td id="name_ar_{{$branch['id']}}">{{$branch['name_ar']}}</td>
                                @else
                                <td id="name_en_{{$branch['id']}}">{{$branch['name_en']}}</td>
                                @endif
                                <td id='branch-existence-{{$branch['id']}}'>
                                    @if($branch->trashed())
                                    <span class='label label-danger'>
                                        @lang('site.deleted')
                                    </span>
                                    @else
                                    <span class='label label-success'>
                                        @lang('site.exist')
                                    </span>
                                    @endif
                                </td>
                                <td id='created_at_{{$branch['id']}}'>{{$branch['created_at']}}</td>
                                <td id='updated_at_{{$branch['id']}}'>
                                    {{$branch['updated_at']}}
                                </td>
                                <td id='branch-actions-{{$branch['id']}}'>
                                    @if($branch->trashed())
                                    <button onclick="requests.restore({{$branch['id']}})" 
                                            class="btn btn-default btn-sm"
                                            @cannot('delete','branches') disabled @endcannot
                                            >
                                            <i class="fa fa-window-restore"></i>
                                    </button>
                                    @else
                                    <button 
                                        data-toggle="modal"
                                        data-target="#update-branch"
                                        id='edit-branch-{{$branch['id']}}'
                                        onclick="requests.getBranch({{$branch['id']}})"
                                        data-url="{{route('branches.edit',$branch['id'])}}"
                                        class="btn btn-default btn-sm"
                                        @cannot('update','branches') disabled @endcannot
                                        >
                                        <i class="fa fa-edit"></i> 
                                    </button>
                                    
                                    <button
                                        onclick="utils.confirmDelete({{$branch['id']}})" 
                                        data-target="#confirm-delete" 
                                        data-toggle="modal" 
                                        class="btn btn-default btn-sm"
                                        @cannot('delete','branches') disabled @endcannot
                                        >
                                        <i class='fa fa-trash'></i>
                                    </button>
                                    
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <button  data-toggle='modal' data-target='#create-branch' class="btn btn-primary"
                         @cannot('create','branches') disabled @endcannot
                         >
                         @lang('site.create')
            </button>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
    {{$branches->links()}}
</div>
</section>
@endsection