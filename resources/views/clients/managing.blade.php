@extends('layouts.app')

@push('client-script')
@include('js.clients.boundary')
@endpush

@section('title',__('site.clients'))

@section('content')

@include('clients.modal',[
"id"=>'create-client',
"action"=>__('site.create'),
"putInput"=>"",
])

@include('clients.modal',[
"id"=>'update-client',
"action"=>__('site.update'),
"putInput"=>"<input type='hidden' name='_method' value='put'>",
])

@include('includes.delete')
<section class="content-header">
    <h1>
        @lang('site.clients')
    </h1>
    <div class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @lang('site.managing') @lang('site.clients')
                </h3>
                <div class="box-tools">
                    <form method="get" action='{{route('clients.search')}}'>
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <input type="text" name="search" class="form-control pull-right" placeholder="@lang('site.search')">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="myTable" class="table no-margin">
                        <thead>
                            <tr>
                                <th>@lang('site.id')</th>
                                <th>@lang('site.name')</th>
                                <th>@lang('site.existence')</th>
                                <th>@lang('site.creation-date')</th>
                                <th>@lang('site.last-update')</th>
                                <th>@lang('site.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)
                            <tr>
                                <td>{{$client['id']}}</td>
                                @if(app()->getLocale()==="ar")
                                <td id="name_ar_{{$client['id']}}">{{$client['name_ar']}}</td>
                                @else
                                <td id="name_en_{{$client['id']}}">{{$client['name_en']}}</td>
                                @endif
                                <td id='existence-{{$client['id']}}'>
                                    @if($client->trashed())
                                    <span class='label label-danger'>
                                        @lang('site.deleted')
                                    </span>
                                    @else
                                    <span class='label label-success'>
                                        @lang('site.exist')
                                    </span>
                                    @endif
                                </td>
                                <td>{{$client['created_at']}}</td>
                                <td id="updated_at_{{$client['id']}}">
                                    {{$client['updated_at']}}
                                </td>
                                <td id="actions-{{$client['id']}}">
                                    @if($client->trashed())
                                    <button 
                                        class="btn btn-default btn-sm"
                                        onclick="requests.restore({{$client['id']}})"
                                        @cannot('delete','clients') disabled @endcannot
                                        >
                                        <i class="fa fa-window-restore"></i>
                                    </button>
                                    @else
                                    <button 
                                        data-toggle="modal"
                                        data-target="#update-client"
                                        class="btn btn-default btn-sm"
                                        id='edit-client-{{$client['id']}}'
                                        onclick="requests.getClient({{$client['id']}})"
                                        data-url="{{route('clients.edit',$client['id'])}}"
                                        @cannot('update','clients') disabled @endcannot
                                        >
                                        <i class="fa fa-edit"></i> 
                                    </button>
                                    <button
                                        onclick="utils.confirmDelete({{$client['id']}})"
                                        data-target="#confirm-delete" 
                                        data-toggle="modal" 
                                        class="btn btn-default btn-sm"
                                        @cannot('delete','clients') disabled @endcannot
                                        >
                                        <i class='fa fa-trash'></i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <button 
                    class="btn btn-primary" 
                    data-target="#create-client" 
                    data-toggle="modal"
                    @cannot('create','clients') disabled @endcannot
                    >
                    @lang('site.create')
            </button>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
    {{$clients->links()}}
</div>
</section>

@endsection('content')
