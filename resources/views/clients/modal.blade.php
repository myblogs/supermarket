<div id="{{$id}}" class="modal fade" role="dialog">
    <form id="form">
        @csrf
        {!! $putInput !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{$action}}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>@lang('site.name-ar')</label>
                        <input class="form-control" name="name_ar" type="text"/>
                        <div id="name-ar-error" class="text-danger">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>@lang('site.name-en')</label>
                        <input class="form-control" name="name_en" type="text"/>
                        <div id="name-en-error" class="text-danger">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="{{$action}}">
                    <a class="btn btn-warning" data-dismiss="modal">@lang('site.close')</a>
                </div>
            </div>
        </div>
    </form>
</div>