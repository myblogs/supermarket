<div id="confirm-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('site.delete')</h4>
            </div>
            <div class="modal-body">
                <div class="text-danger">
                    @lang('site.confirm-delete')
                </div>
            </div>
            <div class="modal-footer">
                <button id="button" type="button" class="btn btn-danger">@lang('site.confirm')</button>
                <a class="btn btn-warning" data-dismiss="modal">@lang('site.close')</a>
            </div>
        </div>
    </div>
</div>
