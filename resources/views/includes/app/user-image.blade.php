<div id="user-image" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form id="user-image-form" >
            @csrf
            @method('put')
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('site.user-image')</h4>
                </div>
                <div class="modal-body">
                    <div class='image'>
                        <span class="btn btn-default btn-block btn-file">
                            @lang('site.choose-image')
                            <input id="image-file" type="file" name="image" />
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button  id="button" type="button" class="btn btn-primary">@lang('site.update')</button>
                    <a style="margin-left:auto;margin-right: auto" class="btn btn-warning" data-dismiss="modal">@lang('site.close')</a>
                </div>
            </div>
        </form>
    </div>
</div>