<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset($currentUser->image)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p id="user-name">{{$currentUser->name}}</p>
                <p><i class="fa fa-circle text-success"></i> @lang('site.online')</p>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="@lang('site.search')">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">@lang('site.main-navigation')</li>
            <li class="treeview menu-close">
                <a href="#">
                    <i class="fa fa-globe" aria-hidden="true"></i> <span>@lang('site.languages')</span>
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{route('locale',"ar")}}"><i class="fa fa-circle-o"></i> @lang('site.ar')</a></li>
                    <li><a href="{{route('locale',"en")}}"><i class="fa fa-circle-o"></i> @lang('site.en')</a></li>
                </ul>
            </li>
            @can('view','branches')
            <li>
                <a href="{{route("branches.index")}}">
                    <i class="fa fa-files-o"></i>
                    <span>@lang('site.branches')</span>
                </a>
            </li>
            @endcan
            @can("view","clients")
            <li>
                <a href="{{route("clients.index")}}">
                    <i class="fa fa-files-o"></i>
                    <span>@lang('site.clients')</span>
                </a>
            </li>
            @endcan

            @can("view","items")
            <li>
                <a href="{{route("items.index")}}">
                    <i class="fa fa-files-o"></i>
                    <span>@lang('site.items')</span>
                </a>
            </li>
            @endcan
            @can('view','warehouses') 
            <li>
                <a href="{{route("warehouses.index")}}">
                    <i class="fa fa-files-o"></i>
                    <span>@lang('site.warehouses')</span>
                </a>
            </li>
            @endcan

            @can('view','users')
            <li>
                <a href="{{route("users.index")}}">
                    <i class="fa fa-users"></i>
                    <span>@lang('site.users')</span>
                </a>

            </li>
            @endcan
            <li>
                <a href="{{route("users.profile")}}">
                    <i class="fa fa-user"></i>
                    <span>@lang('site.profile')</span>
                </a>
            </li>

            <li>
                <a href="{{route('app-logout')}}">
                    <i class="fa fa-sign-out"></i> <span>@lang('site.logout')</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>