<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/skin-blue.css') }}">
<link rel="stylesheet" href="{{asset('css/app.css')}}">
@if(app()->getLocale()==="ar")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.min.css">
<link rel="stylesheet" href="{{asset('css/AdminLTE-rtl.min.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome-rtl.min.css')}}">
<link rel="stylesheet" href="{{asset('css/app.rtl.css')}}">
@else
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/AdminLTE.css') }}">
@endif
@stack('users-styles')