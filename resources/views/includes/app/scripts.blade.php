<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="{{asset('js/adminlte.min.js')}}"></script>
<script src="{{asset('js/notify.min.js')}}"></script>
@stack('branch-scripts')
@stack('user-scripts')
@stack('client-script')
@stack('items-script')
@stack('warehouse-script')
