<script>
    var utils = {
        emptyInvalidationErrors: function () {
            $("#name-ar-error").text("");
            $("#name-en-error").text("");
            $("#email-error").text("");
            $("#password-error").text("");
            $("#phone-error").text("");
            $("#branch-error").text("");
        },
        throwInvalidationErrors: function (data) {
            var errors = JSON.parse(data.responseText).errors;
            var name_ar_error = (errors.name_ar !== undefined) ? errors.name_ar[0] : "";
            var name_en_error = (errors.name_en !== undefined) ? errors.name_en[0] : "";
            var email_error = (errors.email !== undefined) ? errors.email[0] : "";
            var password_error = (errors.password !== undefined) ? errors.password[0] : "";
            var phone_error = (errors.phone !== undefined) ? errors.phone[0] : "";
            var branch_error = (errors.branch_id !== undefined) ? errors.branch_id[0] : "";
            $("#name-ar-error").text(name_ar_error);
            $("#name-en-error").text(name_en_error);
            $("#email-error").text(email_error);
            $("#password-error").text(password_error);
            $("#phone-error").text(phone_error);
            $("#branch-error").text(branch_error);
        }
    };
</script>