@include('js.auth.utils')
<script>
    var requests = {
        register: function () {
            $("#register-button").click(function (e) {
                e.preventDefault();
                utils.emptyInvalidationErrors();
                $(".login-box-msg")
                    .after("<div id='spinner' class='text-center'>@lang('site.loading')</div>");

                $.ajax({
                    type: "post",
                    url: "{{route('register')}}",
                    data: {
                        name_ar: $("#name_ar").val(),
                        name_en: $("#name_en").val(),
                        email: $("#email").val(),
                        password: $("#password").val(),
                        password_confirmation: $("#password_confirmation").val(),
                        phone: $("#phone").val(),
                        branch_id: $("#branch_id").val(),
                        _token: "{{csrf_token()}}",
                    },
                    success: function () {
                    },
                    error: function (data) {
                        if (data.status === 403) {
                            //if email not verified
                            window.location = "{{route('branches.index')}}";
                        } else {
                            $("#spinner").remove();
                            utils.throwInvalidationErrors(data);
                        }
                    }
                });
            });
        }
    }
</script>
