@include('js.branches.utils')
<script>
    var requests = {
        create: function () {
            $("#create-branch #button").click(function (e) {
                e.preventDefault();
                var form = $("#create-branch form");
                $.ajax({
                    type: form.attr("data-method"),
                    url: form.attr("data-url"),
                    data: utils.getData("#create-branch"),
                    success: function (data) {
                        utils.emptyInvalidationErrors(data, "#create-branch");
                        utils.updateTableAfterCreate(data);
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors(data, "#create-branch");
                    }
                });
            });
        },
        //end create method
        getBranch: function (id) {
            $("#update-branch .modal-header")
                    .append("<i class='fa fa-refresh fa-spin fa-lg text-info'></i>")
            $.ajax({
                type: "get",
                url: $("#edit-branch-" + id).attr("data-url"),
                success: function (branch) {
                    $("#update-branch .modal-header").children("i").hide();
                    $("#update-branch input[name='name_ar']").val(branch.name_ar);
                    $("#update-branch input[name='name_en']").val(branch.name_en);
                    $("#update-branch form").attr("branch-id", branch.id);
                }
            });
        },
        //end getBranch function
        update: function () {
            $("#update-branch #button").click(function (e) {
                e.preventDefault();
                var form = $("#update-branch form");
                var branchId = form.attr("branch-id");
                $.ajax({
                    type: form.attr("data-method"),
                    url: '{{route("branches.update",1)}}'.replace("1", branchId),
                    data: utils.getData("#update-branch"),
                    success: function (data) {
                        utils.emptyInvalidationErrors(data, "#update-branch");
                        utils.updateTableAfterUpdate(data, branchId);
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors(data, "#update-branch");
                    }
                });
            });
        },
        //end update function
        delete: function () {
            $("#confirm-delete #button").click(function () {
                var branchId = $(this).attr("data-id");
                $.ajax({
                    type: "get",
                    url: "{{route('branches.delete',1)}}".replace("1", branchId),
                    success: function (data) {
                        utils.updateTableAfterDelete(branchId);
                        $.notify(data, "success");
                    }
                });
            });

        },
        //end delete function
        restore: function (id) {
            $.ajax({
                type: "get",
                url: "{{route('branches.restore',1)}}".replace("1", id),
                success: function (data) {
                    utils.updateTableAfterRestore(id);
                }
            });
        }
        //end restore function

    };
</script>