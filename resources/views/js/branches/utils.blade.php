<script>
    var utils = {
        updateTableAfterCreate: function (data) {
            var name;
            var nameAttrId;
            if ("{{app() -> getLocale()}}" === "ar") {
                name = data.branch.name_ar;
                nameAttrId = "name_ar_" + data.branch.id;
            } else {
                name = data.branch.name_en;
                nameAttrId = "name_en_" + data.branch.id;
            }
            if (($("#myTable tr").length - 1) < data.page_size) {
                var id = data.branch.id;
                $("#myTable tbody").append(
                        "<tr>" +
                        "<td>" + id + "</td>" +
                        "<td id='*'>".replace("*",nameAttrId) + name + "</td>" +
                        "<td id='branch-existence-" + id + "'></td>" +
                        "<td>" + data.branch.created_at + "</td>" +
                        "<td id='updated_at-" + id + "'>" + data.branch.updated_at + "</td>" +
                        "<td id='branch-actions-" + id + "'></td>" +
                        "</tr>"
                        );
                this.updateTableAfterRestore(id);
            }
        },
        //end updateTableAfterCreate
        confirmDelete: function (id) {
            $("#confirm-delete #button").attr("data-id", id);
        },
        //end confirmDelete
        updateTableAfterDelete: function (branchId) {
            var onclick = "'requests.restore(" + branchId + ")'";
            $("#branch-actions-" + branchId).html(
                    " <button" +
                    " class='btn btn-default btn-sm'" +
                    " onclick=" + onclick +
                    ">" +
                    " <i class='fa fa-window-restore'></i>" +
                    " </button>"
                    );
            $("#branch-existence-" + branchId).html(
                    "<span class='label label-danger'>" +
                    "@lang('site.deleted')" +
                    "</span>"
                    );
        },
        //end updateTableAfterDelete
        updateTableAfterRestore: function (id) {
            var updateProperties = {
                data_toggle: "'modal'",
                data_target: "'#update-branch'",
                id: "'edit-branch-" + id + "'",
                onclick: "'requests.getBranch(" + id + ")'",
                data_url: "'{{route('branches.edit',1)}}".replace("1", id) + "'",
                clss: "'btn btn-default btn-sm'",
            };
            var deleteProperties = {
                data_toggle: "'modal'",
                data_target: "'#confirm-delete'",
                onclick: "'utils.confirmDelete(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            $("#branch-actions-" + id).html(
                    " <button" +
                    " data-toggle=" + updateProperties.data_toggle +
                    " data-target=" + updateProperties.data_target +
                    " id=" + updateProperties.id +
                    " onclick=" + updateProperties.onclick +
                    " data-url=" + updateProperties.data_url +
                    " class=" + updateProperties.clss +
                    "@cannot('update','branches') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-edit'></i>" +
                    "</button>" +
                    " <button" +
                    " onclick=" + deleteProperties.onclick +
                    " data-target=" + deleteProperties.data_target +
                    " data-toggle=" + deleteProperties.data_toggle +
                    " class=" + deleteProperties.clss +
                    "@cannot('delete','branches') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-trash'></i>" +
                    "</button>"
                    );
            $("#branch-existence-" + id).html(
                    "<span class='label label-success'>" +
                    "@lang('site.exist')" +
                    "</span>"
                    );
        },
        //end updateTableAfterRestore
        throwInvalidationErrors: function (data, id) {
            var errors = JSON.parse(data.responseText).errors;
            var errorArName = (errors.name_ar !== undefined) ? errors.name_ar[0] : "";
            var errorEnName = (errors.name_en !== undefined) ? errors.name_en[0] : "";
            $(id + " #name_ar_error").text(errorArName);
            $(id + " #name_en_error").text(errorEnName);
        },
        //end throwErrors
        emptyInvalidationErrors: function (data, id) {
            $(id + " #name_ar_error").text("");
            $(id + " #name_en_error").text("");
        },
        //end throwSuccess
        getData: function (id) {
            return {
                name_ar: $(id + " form input[name='name_ar']")
                        .val(),
                name_en: $(id + " form input[name='name_en']")
                        .val(),
                _token: $(id + " form").attr("data-token")
            };
        },
        //end getData
        updateTableAfterUpdate: function (data, branchId) {
            $("#name_ar_" + branchId).text(data.branch.name_ar);
            $("#name_en_" + branchId).text(data.branch.name_en);
            $("#updated_at_" + branchId).text("{{trans_choice('site.second',0)}}");
        }
        //end updateTableAfterUpdate
    };
</script>

