@include('js.warehouses.utils')
<script>
    var requests = {
        create: function () {
            $("#create-warehouse #form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('warehouses.store')}}",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors("#create-warehouse");
                        utils.updateTableAfterCreate(data);
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors("#create-warehouse", data);
                    }
                });
            });
        },
        getWarehouse: function (id) {
            $("#update-warehouse .modal-header")
                    .append("<i class='fa fa-refresh fa-spin fa-lg text-info'></i>")
            $.ajax({
                type: "get",
                url: "{{route('warehouses.edit',1)}}".replace("1", id),
                success: function (data) {
                    $("#update-warehouse .modal-header").children("i").remove();
                    $("#update-warehouse #form").attr("data-id", id);
                    utils.updateEditFormAfterGetWarehouse(data);
                },
            });
        },

        update: function () {
            var form = $("#update-warehouse #form");
            form.submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('warehouses.update',1)}}".replace("1"
                            , form.attr("data-id")),
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors("#update-warehouse");
                        utils.updateTableAfterUpdate(data, form.attr("data-id"));
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors("#update-warehouse", data);
                    }
                });
            });
        },
        delete: function () {
            $("#confirm-delete #button").click(function () {
                var id = $(this).attr("data-id");
                $.ajax({
                    type: "get",
                    url: "{{route('warehouses.delete',1)}}".replace("1", id),
                    success: function (data) {
                        utils.updateTableAfterDelete(id);
                        $.notify(data, "success");
                    }
                });
            });
        },
        restore: function (id) {
            $.ajax({
                type: "get",
                url: "{{route('warehouses.restore',1)}}".replace("1", id),
                success: function (data) {
                    utils.updateTableAfterRestore(id);
                }
            });
        }
    };
</script>