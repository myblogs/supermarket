<script>
    var utils = {
        throwInvalidationErrors: function (id, data) {
            var errors = JSON.parse(data.responseText).errors;
            var errorArName = (errors.name_ar !== undefined) ? errors.name_ar[0] : "";
            var errorEnName = (errors.name_en !== undefined) ? errors.name_en[0] : "";
            var errorbranchId = (errors.branch_id !== undefined) ? errors.branch_id[0] : "";
            $(id + " #name-ar-error").text(errorArName);
            $(id + " #name-en-error").text(errorEnName);
            $(id + " #branch-id-error").text(errorbranchId);
        },
        emptyInvalidationErrors: function (id) {
            $(id + " #name-ar-error").text("");
            $(id + " #name-en-error").text("");
            $(id + " #branch-id-error").text("");
        },
        updateEditFormAfterGetWarehouse: function (data) {
            $("#update-warehouse #form input[name='name_ar']")
                    .val(data.name_ar);
            $("#update-warehouse #form input[name='name_en']")
                    .val(data.name_en);
            $("#update-warehouse #form select option").filter(function () {
                if ($(this).val() == data.branch_id) {
                    $(this).attr("selected", "true");
                } else {
                    $(this).removeAttr("selected");
                }
                
            });
            
        },
        updateTableAfterUpdate: function (data, id) {
            $("#myTable #" + id + " td")[1].textContent = data.warehouse.name;
            $("#myTable #" + id + " td")[3].textContent = data.warehouse.branch.name;
            $("#myTable #" + id + " td")[5].textContent = data.warehouse.updated_at;
        },
        confirmDelete: function (id) {
            $("#confirm-delete #button").attr("data-id", id);
        },
        updateTableAfterDelete: function (id) {
            var onclick = "'requests.restore(" + id + ")'";
            $("#myTable #" + id + " td")[6].innerHTML =
                    " <button" +
                    " class='btn btn-default btn-sm'" +
                    " onclick=" + onclick +
                    ">" +
                    " <i class='fa fa-window-restore'></i>" +
                    " </button>";
            $("#myTable #" + id + " td")[2].innerHTML =
                    "<span class='label label-danger'>" +
                    "@lang('site.deleted')" +
                    "</span>";
        },
        updateTableAfterRestore: function (id) {
            var updateProperties = {
                data_toggle: "'modal'",
                data_target: "'#update-warehouse'",
                onclick: "'requests.getWarehouse(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            var deleteProperties = {
                data_toggle: "'modal'",
                data_target: "'#confirm-delete'",
                onclick: "'utils.confirmDelete(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            $("#myTable #" + id + " td")[6].innerHTML =
                    " <button" +
                    " data-toggle=" + updateProperties.data_toggle +
                    " data-target=" + updateProperties.data_target +
                    " onclick=" + updateProperties.onclick +
                    " class=" + updateProperties.clss +
                    "@cannot('update','warehouses') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-edit'></i>" +
                    "</button>" +
                    " <button" +
                    " onclick=" + deleteProperties.onclick +
                    " data-target=" + deleteProperties.data_target +
                    " data-toggle=" + deleteProperties.data_toggle +
                    " class=" + deleteProperties.clss +
                    "@cannot('delete','warehouses') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-trash'></i>" +
                    "</button>";
            $("#myTable #" + id + " td")[2].innerHTML =
                    "<span class='label label-success'>" +
                    "@lang('site.exist')" +
                    "</span>";
        },
        updateTableAfterCreate: function (data) {
            if (($("#myTable tr").length - 1) < data.page_size) {
                var id = data.warehouse.id;
                $("#myTable tbody").append(
                        "<tr id ='*'>".replace("*", id) +
                        "<td>" + id + "</td>" +
                        "<td>" + data.warehouse.name + "</td>" +
                        "<td></td>" +
                        "<td>" + data.warehouse.branch.name + "</td>" +
                        "<td>" + data.warehouse.created_at + "</td>" +
                        "<td>" + data.warehouse.updated_at + "</td>" +
                        "<td></td>" +
                        "</tr>"
                        );
                this.updateTableAfterRestore(id);
            }
        },
    };
</script>

