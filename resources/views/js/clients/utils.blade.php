<script>
    var utils = {
        emptyInvalidationErrors: function (id) {
            $(id + " #name-ar-error").text("");
            $(id + " #name-en-error").text("");
        },
        
        throwInvalidationErrors: function (id, data) {
            var errors = JSON.parse(data.responseText).errors;
            var errorArName = (errors.name_ar !== undefined) ? errors.name_ar[0] : "";
            var errorEnName = (errors.name_en !== undefined) ? errors.name_en[0] : "";
            $(id + " #name-ar-error").text(errorArName);
            $(id + " #name-en-error").text(errorEnName);
        },
        updateTableAfterCreate: function (data) {
            var name;
            var nameAttrId;
            if ("{{app() -> getLocale()}}" === "ar") {
                name = data.client.name_ar;
                nameAttrId = "name_ar_" + data.client.id;
            } else {
                name = data.client.name_en;
                nameAttrId = "name_en_" + data.client.id;
            }
            if (($("#myTable tr").length - 1) < data.page_size) {
                var id = data.client.id;
                $("#myTable tbody").append(
                        "<tr>" +
                        "<td>" + id + "</td>" +
                        "<td id='*'>".replace("*", nameAttrId) + name + "</td>" +
                        "<td id='existence-" + id + "'></td>" +
                        "<td>{{trans_choice('site.second',0)}}</td>" +
                        "<td id='updated_at-" + id + "'>" +
                        "{{trans_choice('site.second',0)}}" +
                        "</td>" +
                        "<td id='actions-" + id + "'></td>" +
                        "</tr>"
                        );
                this.updateTableAfterRestore(id);
            }
        },
        updateTableAfterUpdate: function (data, clientId) {
            $("#name_ar_" + clientId).text(data.client.name_ar);
            $("#name_en_" + clientId).text(data.client.name_en);
            $("#updated_at_" + clientId).text("{{trans_choice('site.second',0)}}");
        },
        confirmDelete: function (id) {
            $("#confirm-delete #button").attr("data-id", id);
        },
        updateTableAfterDelete: function (clientId) {
            var onclick = "'requests.restore(" + clientId + ")'";
            $("#actions-" + clientId).html(
                    " <button" +
                    " class='btn btn-default btn-sm'" +
                    " onclick=" + onclick +
                    ">" +
                    " <i class='fa fa-window-restore'></i>" +
                    " </button>"
                    );
            $("#existence-" + clientId).html(
                    "<span class='label label-danger'>" +
                    "@lang('site.deleted')" +
                    "</span>"
                    );
        },
        updateTableAfterRestore: function (id) {
            var updateProperties = {
                data_toggle: "'modal'",
                data_target: "'#update-client'",
                id: "'edit-client-" + id + "'",
                onclick: "'requests.getClient(" + id + ")'",
                data_url: "'{{route('clients.edit',1)}}".replace("1", id) + "'",
                clss: "'btn btn-default btn-sm'",
            };
            var deleteProperties = {
                data_toggle: "'modal'",
                data_target: "'#confirm-delete'",
                onclick: "'utils.confirmDelete(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            $("#actions-" + id).html(
                    " <button" +
                    " data-toggle=" + updateProperties.data_toggle +
                    " data-target=" + updateProperties.data_target +
                    " id=" + updateProperties.id +
                    " onclick=" + updateProperties.onclick +
                    " data-url=" + updateProperties.data_url +
                    " class=" + updateProperties.clss +
                    "@cannot('update','clients') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-edit'></i>" +
                    "</button>" +
                    " <button" +
                    " onclick=" + deleteProperties.onclick +
                    " data-target=" + deleteProperties.data_target +
                    " data-toggle=" + deleteProperties.data_toggle +
                    " class=" + deleteProperties.clss +
                    "@cannot('delete','clients') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-trash'></i>" +
                    "</button>"
                    );
            $("#existence-" + id).html(
                    "<span class='label label-success'>" +
                    "@lang('site.exist')" +
                    "</span>"
                    );
        },
    };
</script>

