@include('js.clients.utils')
<script>
    var requests = {
        create: function () {
            $("#create-client #form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('clients.store')}}",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors("#create-client");
                        utils.updateTableAfterCreate(data);
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors("#create-client", data);
                    }
                });
            });
        },
        
        update: function () {
            var form = $("#update-client #form");
            form.submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('clients.update',1)}}".replace("1"
                            , form.attr('client-id')),
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors("#update-client");
                        utils.updateTableAfterUpdate(data
                                , form.attr("client-id"));
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors("#update-client", data);
                    }
                });
            });
        },
        getClient: function (id) {
            $("#update-client .modal-header")
                    .append("<i class='fa fa-refresh fa-spin fa-lg text-info'></i>")
            $.ajax({
                type: "get",
                url: $("#edit-client-" + id).attr("data-url"),
                success: function (client) {
                    $("#update-client .modal-header").children("i").hide();
                    $("#update-client input[name='name_ar']").val(client.name_ar);
                    $("#update-client input[name='name_en']").val(client.name_en);
                    $("#update-client form").attr("client-id", client.id);
                }
            });
        },
        delete: function () {
            $("#confirm-delete #button").click(function () {
                var clientId = $(this).attr("data-id");
                $.ajax({
                    type: "get",
                    url: "{{route('clients.delete',1)}}".replace("1", clientId),
                    success: function (data) {
                        utils.updateTableAfterDelete(clientId);
                        $.notify(data, "success");
                    }
                });
            });
        },
                
        restore: function (id) {
            $.ajax({
                type: "get",
                url: "{{route('clients.restore',1)}}".replace("1", id),
                success: function (data) {
                    utils.updateTableAfterRestore(id);
                }
            });
        }
    };
</script>