@include('js.users.utils')
<script>
    var requests = {
        delete: function () {
            $("#confirm-delete #button").click(function () {
                var id = $(this).attr("data-id");
                $.ajax({
                    type: "get",
                    url: "{{route('users.delete',1)}}".replace("1", id),
                    success: function (data) {
                        utils.updateTableAfterDelete(id);
                        $.notify(data, "success");
                    }
                });
            });
        },
        //end delete function
        restore: function (id) {
            $.ajax({
                type: "get",
                url: "{{route('users.restore',1)}}".replace("1", id),
                success: function (data) {
                    utils.updateTableAfterRestore(id);
                }
            });
        },
        //end restore function
        setRoles: function () {
            $("#authorizations #button").click(function () {
                var id = $(this).attr("data-id");
                $.ajax({
                    type: "post",
                    url: "{{route('users.set_roles',1)}}".replace("1", id),
                    data: utils.prepareRolesData("#authorizations input"),
                    success: function (data) {
                        $.notify(data, "success");
                    },
                });
            });
        },
        //end setRoles function
        showRoles: function (id) {
            $("#authorizations .modal-header")
                    .append("<i class='fa fa-refresh fa-spin fa-lg text-info'></i>")
            $.ajax({
                type: "get",
                url: "{{route('users.show_roles',1)}}".replace("1", id),
                success: function (roles) {
                    $("#authorizations .modal-header").children("i").hide();
                    $("#authorizations #button").attr("data-id", id);
                    utils.viewUserRoles("#authorizations .modal-body", roles);
                }
            });
        },
        updateProfile: function () {
            $("#profile-update-form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('users.profile')}}",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors();
                        utils.updatePageAfterUpdate(data);
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors(data);
                    }
                });
            });
        }
    };
</script>