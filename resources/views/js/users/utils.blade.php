<script>
    var utils = {
        confirmDelete: function (id) {
            $("#confirm-delete #button").attr("data-id", id);
        },
        //end confirmDelete function
        updateTableAfterDelete: function (id) {
            var onclick = "'requests.restore(" + id + ")'";
            $("#actions-" + id).html(
                    " <button" +
                    " class='btn btn-default btn-sm'" +
                    " onclick=" + onclick +
                    ">" +
                    " <i class='fa fa-window-restore'></i>" +
                    " </button>"
                    );
            $("#existence-" + id).html(
                    "<span class='label label-danger'>" +
                    "@lang('site.deleted')" +
                    "</span>"
                    );
        },
        //end updateTableAfterDelete
        updateTableAfterRestore: function (id) {
            var authPorperities = {
                data_toggle: "'modal'",
                data_target: "'#authorizations'",
                onclick: "'requests.showRoles(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            var deleteProperties = {
                data_toggle: "'modal'",
                data_target: "'#confirm-delete'",
                onclick: "'utils.confirmDelete(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            $("#actions-" + id).html(
                    " <button" +
                    " onclick=" + authPorperities.onclick +
                    " data-target=" + authPorperities.data_target +
                    " data-toggle=" + authPorperities.data_toggle +
                    "@cannot('update','users') disabled @endcannot" +
                    " class=" + authPorperities.clss +
                    " >" +
                    "<i class='fa fa-lock'></i>" +
                    "</button>" +
                    " <button" +
                    " onclick=" + deleteProperties.onclick +
                    " data-target=" + deleteProperties.data_target +
                    " data-toggle=" + deleteProperties.data_toggle +
                    " class=" + deleteProperties.clss +
                    " >" +
                    "<i class='fa fa-trash'></i>" +
                    "</button>"
                    );
            $("#existence-" + id).html(
                    "<span class='label label-success'>" +
                    "@lang('site.exist')" +
                    "</span>"
                    );
        },
        //end updateTableAfterRestore

        prepareRolesData: function (inputs) {
            var data = [];
            $(inputs).filter(function () {
                if ($(this).is(":checked")) {
                    data.push($(this).val());
                }
            });
            return {
                roles: data,
                _token: "{{csrf_token()}}"
            };
        },
        //end prepareRolesDate
        viewUserRoles: function (modalBody, roles) {
            var content = "";
            var model_roles = {
                "{{__('site.users')}}": [1, 2, 3, 4],
                "{{__('site.branches')}}": [5, 6, 7, 8],
                "{{__('site.clients')}}": [9, 10, 11, 12],
                "{{__('site.items')}}": [13, 14, 15, 16],
                "{{__('site.warehouses')}}": [17, 18, 19, 20]
            };
            var roleName = [
                "{{__('site.create')}}",
                "{{__('site.update')}}",
                "{{__('site.delete')}}",
                "{{__('site.view')}}"
            ];
            for (var key  in model_roles) {
                content += "<label>" + key + "</label>" +
                        "<div class='authorizations'>";
                for (var _key in model_roles[key]) {
                    var checked = (roles.some(role => role.id
                                === model_roles[key][_key])) ? "checked" : "";
                    content +=
                            "<label>" +
                            "<input type='checkbox' value='" +
                            model_roles[key][_key] + "'" +
                            checked + "/> " +
                            roleName[_key] +
                            "</label>";
                }
                content += "</div>" + "<hr/>";

            }
            $(modalBody).html(content);
        },
        emptyInvalidationErrors: function () {
            $("#profile-update-form #name-ar-error").text("");
            $("#profile-update-form #name-en-error").text("");
            $("#profile-update-form #phone-error").text("");
            $("#profile-update-form #branch-error").text("");
        },
        updatePageAfterUpdate: function (data) {
            $("#user-name").text(data.updatedName);
            $("#profile-image span").text(data.updatedName);
        },
        throwInvalidationErrors: function (data) {
            var errors = JSON.parse(data.responseText).errors;
            var errorArName = (errors.name_ar !== undefined) ? errors.name_ar[0] : "";
            var errorEnName = (errors.name_en !== undefined) ? errors.name_en[0] : "";
            var errorPhone = (errors.phone !== undefined) ? errors.phone[0] : "";
            var errorBranch = (errors.branch_id !== undefined) ? errors.branch_id[0] : "";
            $("#profile-update-form #name-ar-error").text(errorArName);
            $("#profile-update-form #name-en-error").text(errorEnName);
            $("#profile-update-form #phone-error").text(errorPhone);
            $("#profile-update-form #branch-error").text(errorBranch);
        }
    };
</script>

