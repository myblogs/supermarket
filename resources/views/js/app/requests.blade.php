@include('js.app.utils')
<script>
    var appRequests = {
        editImage: function () {
            $("#user-image #button").click(function () {
                $.ajax({
                    type: "post",
                    url: "{{route('users.edit-image')}}",
                    data: new FormData(document.getElementById("user-image-form")),
                    dataType: 'JSON',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $.notify(data.message, "success");
                        appUtils.updateAllPageImages(data);
                    },
                    error: function (data) {
                        var errors = JSON.parse(data.responseText).errors;
                        $.notify(errors.image[0], "error");
                    }
                });
            });
        }
    };
</script>