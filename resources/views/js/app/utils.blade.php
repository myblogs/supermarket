<script>
    var appUtils = {
        getUserImage: function () {
            $("#profile-image").click(function () {
                $("#user-image .modal-body .image").children("img").remove();
                $("#user-image .modal-body .image")
                        .prepend("<img src='*' class='user-image' alt='User Image'>"
                                .replace("*", $("#profile-image img").attr("src")));
            });
        },
        image_upload_preview: function () {
            $("#user-image #image-file").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#user-image img').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(this.files[0]);
                }
            });
        },
        updateAllPageImages: function (data) {
            $(".main-header #profile-image img").attr("src"
                    , "{{asset(1)}}".replace("1", data.uploaded_image));
            $("#user-image img").attr("src"
                    , "{{asset(1)}}".replace("1", data.uploaded_image));
            $(".main-sidebar img").attr("src"
                    , "{{asset(1)}}".replace("1", data.uploaded_image));
        }

    };
</script>

