@include('js.items.utils')
<script>
    var requests = {
        create: function () {
            $("#create-item #form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('items.store')}}",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors("#create-item");
                        utils.updateTableAfterCreate(data);
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors(data, "#create-item");
                    }
                });
            });
        },
        getItem: function (id) {
            $("#update-item .modal-header")
                    .append("<i class='fa fa-refresh fa-spin fa-lg text-info'></i>")
            $.ajax({
                type: "get",
                url: "{{route('items.edit',1)}}".replace("1", id),
                success: function (data) {
                    $("#update-item .modal-header").children("i").remove();
                    $("#update-item #form").attr("data-id", id);
                    utils.emptyInvalidationErrors("#update-item");
                    utils.updateEditFormAfterGetItem(data);
                },
            });
        },
                
        update: function () {
            var form = $("#update-item #form");
            form.submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "{{route('items.update',1)}}".replace("1"
                            , form.attr("data-id")),
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        utils.emptyInvalidationErrors("#update-item");
                        utils.updateTableAfterUpdate(data, form
                                .attr("data-id"));
                        $.notify(data.message, "success");
                    },
                    error: function (data) {
                        utils.throwInvalidationErrors(data, "#update-item");
                    }
                });
            });
        },
        delete: function () {
            $("#confirm-delete #button").click(function () {
                var id = $(this).attr("data-id");
                $.ajax({
                    type: "get",
                    url: "{{route('items.delete',1)}}".replace("1", id),
                    success: function (data) {
                        utils.updateTableAfterDelete(id);
                        $.notify(data, "success");
                    }
                });
            });
        },
        restore: function (id) {
            $.ajax({
                type: "get",
                url: "{{route('items.restore',1)}}".replace("1", id),
                success: function (data) {
                    utils.updateTableAfterRestore(id);
                }
            });
        }
    };
</script>