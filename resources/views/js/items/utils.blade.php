<script>
    var utils = {
        togglePriceInputEnabling: function (checkbox, id) {
            var priceInput = $(id + " #form input[name='selling_price']");
            if (checkbox.is(":checked")) {
                priceInput.removeAttr("disabled");
                checkbox.val("1");
            } else {
                priceInput.attr("disabled", "disabled");
                priceInput.val("");
            }
        },
        addUnit: function (id) {
            $(id + " #add").click(function () {
                var length = $(id + " .unit").length;
                $(id + " #units").append(
                        "<div id='*' class='unit row'>".replace("*", length) +
                        "<div class='col-md-4'>" +
                        "<div class='form-group'>" +
                        "<label>@lang('site.unit-ar')</label>" +
                        '<input name="unit_ar[]" class="form-control" type="text">' +
                        "<div class='unit_ar_error text-danger'>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "<div class='col-md-4'>" +
                        '<div class="form-group">' +
                        "<label>@lang('site.unit-en')</label>" +
                        '<input name="unit_en[]" class="form-control" type="text">' +
                        "<div class='unit_en_error text-danger'>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "<div class='col-md-4'>" +
                        '<div class="form-group">' +
                        "<label>@lang('site.capacity')</label>" +
                        '<input name ="capacity[]" class="form-control" type="number">' +
                        "<div class='capacity_error text-danger'>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                        );
            });
        },
        removeLastUnit: function (id) {
            $(id + " #remove").click(function () {
                $(id + " #units .unit").last().remove();
            });
        },
        throwInvalidationErrors: function (data, id) {
            var errors = JSON.parse(data.responseText).errors;
            var nameArError = (errors.name_ar !== undefined) ? errors.name_ar[0] : "";
            var nameEnError = (errors.name_en !== undefined) ? errors.name_en[0] : "";
            var minimumQuantityError = (errors.minimum_quantity !== undefined) ? errors.minimum_quantity[0] : "";
            var branchError = (errors.branch_id !== undefined) ? errors.branch_id[0] : "";
            var sellingPriceError = (errors.selling_price !== undefined) ? errors.selling_price[0] : "";
            var smallestUnitArError = (errors.smallest_unit_ar !== undefined) ? errors.smallest_unit_ar[0] : "";
            var smallestUnitEnError = (errors.smallest_unit_en !== undefined) ? errors.smallest_unit_en[0] : "";
            $(id + " #name-ar-error").text(nameArError);
            $(id + " #name-en-error").text(nameEnError);
            $(id + " #minimum-quantity-error").text(minimumQuantityError);
            $(id + " #branch-error").text(branchError);
            $(id + " #selling-price-error").text(sellingPriceError);
            $(id + " #smallest-unit-ar-error").text(smallestUnitArError);
            $(id + " #smallest-unit-en-error").text(smallestUnitEnError);
            this.throwUnitsInvalidationErrors(errors, id);
        },
        emptyInvalidationErrors: function (id) {
            $(id + " #name-ar-error").text("");
            $(id + " #name-en-error").text("");
            $(id + " #minimum-quantity-error").text("");
            $(id + " #branch-error").text("");
            $(id + " #selling-price-error").text("");
            $(id + " #smallest-unit-ar-error").text("");
            $(id + " #smallest-unit-en-error").text("");
            $(id + " .unit_ar_error").text("");
            $(id + " .unit_en_error").text("");
            $(id + " .capacity_error").text("");
        },
        updateEditFormAfterGetItem: function (data) {
            $("#update-item #form input[name='name_ar']")
                    .val(data.item.name_ar);
            $("#update-item #form input[name='name_en']")
                    .val(data.item.name_en);
            $("#update-item #form input[name='minimum_quantity']")
                    .val(data.item.minimum_quantity);
            $("#update-item #form input[name='smallest_unit_ar']")
                    .val(data.smallest_unit.smallest_unit_ar);
            $("#update-item #form input[name='smallest_unit_en']")
                    .val(data.smallest_unit.smallest_unit_en);
            $("#update-item #form select option").filter(function () {
                if ($(this).val() == data.item.branch_id) {
                    $(this).attr("selected", "true");
                } else {
                    $(this).removeAttr("selected");
                }
            });
            this.updateSellingPrice(data);
            this.updateUnits(data);
        },
        updateTableAfterUpdate: function (data, id) {
            $("#myTable #" + id + " td")[1].textContent = data.item.name;
            $("#myTable #" + id + " td")[3].textContent = data.item.minimum_quantity;
            $("#myTable #" + id + " td")[5].textContent = data.item.smallest_unit_name;
            $("#myTable #" + id + " td")[8].textContent = data.item.updated_at;
            this.updateForSellAndPriceFields(data, id);
            this.updateUnitsField(data, id);
        },

        confirmDelete: function (id) {
            $("#confirm-delete #button").attr("data-id", id);
        },
        updateTableAfterDelete: function (id) {
            var onclick = "'requests.restore(" + id + ")'";
            $("#myTable #" + id + " td")[9].innerHTML =
                    " <button" +
                    " class='btn btn-default btn-sm'" +
                    " onclick=" + onclick +
                    "@cannot('delete','items') disabled @endcannot" +
                    ">" +
                    " <i class='fa fa-window-restore'></i>" +
                    " </button>";
            $("#myTable #" + id + " td")[2].innerHTML =
                    "<span class='label label-danger'>" +
                    "@lang('site.deleted')" +
                    "</span>";
        },

        updateTableAfterRestore: function (id) {
            var updateProperties = {
                data_toggle: "'modal'",
                data_target: "'#update-item'",
                onclick: "'requests.getItem(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            var deleteProperties = {
                data_toggle: "'modal'",
                data_target: "'#confirm-delete'",
                onclick: "'utils.confirmDelete(" + id + ")'",
                clss: "'btn btn-default btn-sm'",
            };
            $("#myTable #" + id + " td")[9].innerHTML =
                    " <button" +
                    " data-toggle=" + updateProperties.data_toggle +
                    " data-target=" + updateProperties.data_target +
                    " onclick=" + updateProperties.onclick +
                    " class=" + updateProperties.clss +
                    "@cannot('update','items') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-edit'></i>" +
                    "</button>" +
                    " <button" +
                    " onclick=" + deleteProperties.onclick +
                    " data-target=" + deleteProperties.data_target +
                    " data-toggle=" + deleteProperties.data_toggle +
                    " class=" + deleteProperties.clss +
                    "@cannot('delete','items') disabled @endcannot" +
                    " >" +
                    "<i class='fa fa-trash'></i>" +
                    "</button>";
            $("#myTable #" + id + " td")[2].innerHTML =
                    "<span class='label label-success'>" +
                    "@lang('site.exist')" +
                    "</span>";
        },

        updateTableAfterCreate: function (data) {
            if (($("#myTable tr").length - 1) < data.page_size) {
                var id = data.item.id;
                $("#myTable tbody").append(
                        "<tr id ='*'>".replace("*", id) +
                        "<td>" + id + "</td>" +
                        "<td>" + data.item.name + "</td>" +
                        "<td></td>" +
                        "<td>" + data.item.minimum_quantity + "</td>" +
                        "<td></td>" +
                        "<td>" + data.item.smallest_unit_name + "</td>" +
                        "<td></td>" +
                        "<td>" + data.item.created_at + "</td>" +
                        "<td>" + data.item.updated_at + "</td>" +
                        "<td></td>" +
                        "</tr>"
                        );
                this.updateTableAfterRestore(id);
                this.updateForSellAndPriceFields(data, id);
                this.updateUnitsField(data, id);
            }
        },

        //Utils
        throwUnitsInvalidationErrors: function (errors, id) {
            var length = $(id + " .unit").length;
            for (var i = 0; i < length; i++) {
                var unitArError = (errors['unit_ar.' + i] !== undefined) ? errors['unit_ar.' + i][0] : "";
                var unitEnError = (errors['unit_en.' + i] !== undefined) ? errors['unit_en.' + i][0] : "";
                var capacityError = (errors['capacity.' + i] !== undefined) ? errors['capacity.' + i][0] : "";
                $(id + " #" + i + " .unit_ar_error").text(unitArError);
                $(id + " #" + i + " .unit_en_error").text(unitEnError);
                $(id + " #" + i + " .capacity_error").text(capacityError);
            }
        }
        ,
        updateSellingPrice: function (data) {
            if (data.item.for_sell) {
                $("#update-item #form input[name='selling_price']")
                        .removeAttr("disabled", "true")
                        .val(data.item.selling_price);
                $("#update-item #form #checkbox")
                        .html(
                                "<input" +
                                ' onchange= "utils.togglePriceInputEnabling($(this),*)"'.replace("*", "'#update-item'") +
                                ' name="for_sell" checked type="checkbox"> ' +
                                "@lang('site.for-sell')"
                                );
            } else {
                $("#update-item #form input[name='selling_price']")
                        .val("").attr("disabled", "true");
                $("#update-item #form #checkbox")
                        .html(
                                "<input" +
                                ' onchange= "utils.togglePriceInputEnabling($(this),*)"'.replace("*", "'#update-item'") +
                                ' name="for_sell" type="checkbox"/> ' +
                                "@lang('site.for-sell')"
                                );
            }
        },
        updateUnits: function (data) {
            var units = "";
            for (var i = 0; i < data.units.length; i++) {
                units += "<div id='*' class='unit row'>".replace("*", i) +
                        " <div class='col-md-4'>" +
                        " <div class='form-group'>" +
                        " <label>@lang('site.unit-ar')</label>" +
                        ' <input name="unit_ar[]" class="form-control"' +
                        " value='*'".replace("*", data.units[i]['unit_ar']) +
                        ' type="text">' +
                        " <div class='unit_ar_error text-danger'>" +
                        " </div>" +
                        " </div>" +
                        " </div>" +
                        " <div class='col-md-4'>" +
                        ' <div class="form-group">' +
                        " <label>@lang('site.unit-en')</label>" +
                        ' <input name="unit_en[]" class="form-control"' +
                        " value='*'".replace("*", data.units[i]['unit_en']) +
                        ' type="text">' +
                        " <div class='unit_en_error text-danger'>" +
                        " </div>" +
                        " </div>" +
                        " </div>" +
                        " <div class='col-md-4'>" +
                        ' <div class="form-group">' +
                        " <label>@lang('site.capacity')</label>" +
                        ' <input name ="capacity[]" class="form-control"' +
                        " value='*'".replace("*", data.units[i]['capacity']) +
                        ' type="number">' +
                        " <div class='capacity_error text-danger'>" +
                        " </div>" +
                        " </div>" +
                        " </div>" +
                        " </div>";
            }
            $("#update-item #units").html(units);
        },
        updateForSellAndPriceFields: function (data, id) {
            if (data.item.for_sell) {
                $("#myTable #" + id + " td")[4].innerHTML = data
                        .item.selling_price;
            } else {
                $("#myTable #" + id + " td")[4].innerHTML =
                        "<span class='label label-warning'>" +
                        "@lang('site.not-for-sell')" +
                        "</span>";
            }
        },
        updateUnitsField: function (data, id) {
            var content =
                    '<div class="dropdown">' +
                    '<button class="btn btn-sm btn-default dropdown-toggle"' +
                    'type="button" data-toggle="dropdown">' +
                    "@lang('site.units')" +
                    ' <span class="caret"></span>' +
                    "</button>" +
                    '<ul class="dropdown-menu">';
            for (var key in data.item.all_units) {
                content +=
                        "<li>" +
                        "<a>" +
                        data.item.all_units[key]['name'] + " = " +
                        data.item.all_units[key]['capacity'] + " " +
                        data.item.smallest_unit_name +
                        "</a>" +
                        "</li>";
            }
            content += "</ul></div>";
            $("#myTable #" + id + " td")[6].innerHTML = content;
        }
    };
</script>

